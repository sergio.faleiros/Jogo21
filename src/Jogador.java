import java.util.ArrayList;

public class Jogador {
	
	private String nome;
	private int pontuacaoMonte;
	private int qntdCartas;
	private boolean fimJogo;
	ArrayList<Carta> monte = new ArrayList<Carta>();
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getPontuacaoMonte() {
		return pontuacaoMonte;
	}
	
	public int getQntdCartas() {
		return qntdCartas;
	}

	public boolean isFimJogo() {
		return fimJogo;
	}

	public void setFimJogo() {
		this.fimJogo = true;
	}
	
	public void inserirMonte(Carta carta) {
		monte.add(qntdCartas, carta);
		pontuacaoMonte += carta.getPontos();
		qntdCartas ++;
	}
}