import java.util.Random;

public class Baralho {
	
	private int cartasBaralho[][] = new int[13][4];
	
	public void baralho() {

		for(int i=0;i<Rank.getRanksTamanho();i++) {
			for (int j=0;j<Naipe.getNaipesTamanho();j++) {
				cartasBaralho[i][j] = 0;
				
			}
		}
	}

	public Carta sortear() {
		Random random = new Random();
		boolean novaCarta = false;
		int naipe = 0;
		int rank = 0;
		
		while (novaCarta==false) {
			rank = random.nextInt(Rank.getRanksTamanho());
			naipe = random.nextInt(Naipe.getNaipesTamanho());
			if (cartasBaralho[rank][naipe]==0) {
				cartasBaralho[rank][naipe]=1;
				novaCarta = true;
			}
		}
		Carta carta = new Carta(rank,naipe);
		return carta;
	}
}
