
public class Naipe {

	private static String[] naipes = {"Paus", "Ouros", "Copas", "Espadas"};
	
	public static String getNaipes(int indice) {
		return naipes[indice];
	}
	public static int getNaipesTamanho() {
		return naipes.length-1;
	}
}