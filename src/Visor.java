import java.util.ArrayList;
import java.util.Scanner;

public class Visor {

	private static Baralho baralho = new Baralho();
	private static ArrayList<Jogador> jogadores = new ArrayList<Jogador>();
	static Scanner scanner = new Scanner(System.in);
	private static int quantidadeJogador=0;
	private static int jogada=0;
	private static boolean fimJogo = false;
	
	public static void main(String[] args) {
		
		iniciarJogo();
		String menu = "S";
		
		while (fimJogo==false) {
			
			for(int j=0;j<quantidadeJogador;j++) {
				
				if(jogadores.get(j).isFimJogo()==false) {
					
					System.out.println("");
					System.out.println(jogadores.get(j).getNome()+" e sua rodada");
	
					System.out.println(jogadores.get(j).getNome()+" deseja uma nova carta S/N?");
					menu = scanner.nextLine();
					if (menu.equals("S")||menu.equals("s")){
						sortearCarta(j);
						if (jogadores.get(j).getPontuacaoMonte()>21) {
							jogadores.get(j).setFimJogo();
							System.out.println(jogadores.get(j).getNome()+" voce PERDEU \n");
						}
					}else {
						jogadores.get(j).setFimJogo();
						System.out.println(jogadores.get(j).getNome()+" sua pontuacao final foi:" + jogadores.get(j).getPontuacaoMonte());
					}
				
				}
			}
			fimJogo();
		}
		int maiorPontuacao = jogadores.get(0).getPontuacaoMonte();
		int indiceJogador = 0;
		
		for (int i=0;i<quantidadeJogador;i++) {
			if(maiorPontuacao < jogadores.get(i).getPontuacaoMonte()&&jogadores.get(i).getPontuacaoMonte()<21) {
				maiorPontuacao = jogadores.get(i).getPontuacaoMonte();
				indiceJogador = i;
			}
		}
		System.out.println("JOGADOR "+indiceJogador+"Voce ganhou");
	}
	
	public static void fimJogo() {
		
		int cont=0;
		for (int i=0;i<quantidadeJogador;i++) {
			
			if (jogadores.get(i).isFimJogo()==true) {
				cont++;
			}
		}
		if(cont==quantidadeJogador) {
			fimJogo=true;
		}
	}
	public static void sortearCarta(int indice){
		
		Carta carta;
		carta = baralho.sortear();
		jogadores.get(indice).inserirMonte(carta);
		System.out.println("A "+jogadores.get(indice).getQntdCartas()+ " carta e: "+carta.getCarta());
		System.out.println("Sua pontuacao atual e: " + jogadores.get(indice).getPontuacaoMonte());
		System.out.println("");
	}

	public static void iniciarJogo() {	
		
		String menu = "";
		System.out.println("Quantos jogadores irao participar?");
		menu = scanner.nextLine();
		quantidadeJogador = Integer.parseInt(menu);
		
		for(int i=0;i<quantidadeJogador;i++) {		
			
			Jogador jogador = new Jogador();
			System.out.println("Digite o nome do Jogador "+i);
			menu = scanner.nextLine();
			jogador.setNome(menu);			
			for(int c=0;c<2;c++) {
				Carta cartaSorteada = baralho.sortear();
				jogador.inserirMonte(cartaSorteada);
				System.out.println(jogador.getNome()+" a "+jogador.getQntdCartas()+ " carta e: "+cartaSorteada.getCarta());
			}
			System.out.println(jogador.getNome()+i+" Sua pontuacao atual e: " + jogador.getPontuacaoMonte());
			System.out.println("");
			jogadores.add(i, jogador);
		}
	}
}
