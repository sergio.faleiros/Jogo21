
public class Rank {
	
	private static String[] ranks  = {"Ás", "Dois", "Três", "Quatro", "Cinco", "Seis", "Sete", "Oito", "Nove", "Dez","Q","J","K"};
	private static int[] pontos21 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10,10,10,10};
	
	public static String getRanks(int indice) {
		return ranks[indice];
	}
	public static int getRanksTamanho() {
		return ranks.length -1;
	}
	public static int getPontos21(int indice) {
		return pontos21[indice];
	}
}