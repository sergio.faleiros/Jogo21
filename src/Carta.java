
public class Carta {
	
	private String rank;
	private String naipe;
	private int pontos;
	
	public Carta(int rank, int naipe) {
		this.rank = Rank.getRanks(rank);
		this.naipe = Naipe.getNaipes(naipe);
		this.pontos = Rank.getPontos21(rank);
	}
	
	public String getCarta() {
		return rank+" de "+naipe;
	}
	public int getPontos() {
		return this.pontos;
	}
}